var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicletaFindAll);
router.post('/create', bicicletaController.bicicleta_create);
router.get('/code', bicicletaController.bicicletaFindByCode);
router.delete('/delete', bicicletaController.deleteById);
router.put('/update', bicicletaController.bicicletaUpdateById);

module.exports = router;