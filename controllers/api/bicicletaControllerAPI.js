const { json } = require('express');
var Bicicleta = require('../../models/bicicletaModel');
//const connectRedBicicletaDB = require('../../conectionDB/conectionMongoDB');
//const connectDB = connectRedBicicletaDB.getConnectRedBicicletas();

exports.bicicleta_create = function(req, res){
  var b = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
  b.save((err, bici)=>{
    if(err){
      console.error.bind(console, 'Error!');
      res.status(404).send();
    }
    console.log('its ok ' + bici);
    res.status(200).json({
      bicicletas: bici
    });
  });
}

exports.bicicletaFindAll = async function(req, res){
  /*var b = await Bicicleta.findAll()
  res.status(200).json({
    bicicletas: b
  }); */
  Bicicleta.findAll((err, allBicis)=>{
    if(err){
      console.error.bind(console, 'Ocurred an error');
      res.status(404).send();
    } else{
      console.log(allBicis);
      res.status(200).json({ bicicletas: allBicis});
    }
  });
}

exports.bicicletaFindByCode = async function(req, res){
  /*var b = await Bicicleta.findByCode(req.body.code);
  res.status(200).json({
    bicicleta: b
  }); */
  Bicicleta.findByCode(req.body.code, (err, bici)=>{
    if(err){
      console.error.bind(console, 'Ocurred an error');
      res.status(404).send();
    } else{
      console.log(bici);
      res.status(200).json({bicicleta: bici});
    }
  });
}

exports.bicicletaUpdateById = async function(req, res){
  Bicicleta.findById(req.body._id, (err, bici)=>{
    if(err){
      console.error.bind(console, 'Ocurred an error');
      res.status(404).send();
    } else{
      if(req.body.code){
        bici.code = req.body.code;
      }
      if(req.body.color){
        bici.color = req.body.color;
      }
      if(req.body.modelo){
        bici.modelo = req.body.modelo;
      }
      if(req.body.lat){
        bici.ubicacion[0] = req.body.lat;
      }
      if(req.body.lng){
        bici.ubicacion[1] = req.body.lng;
      }
      bici.save((err, upBici)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
          res.status(404).send();
        } else{
          console.log(upBici);
          res.status(200).json({bicicleta: upBici});
        }
      });
    }
  });
}

exports.deleteById = async function(req, res){
  console.log('delete by _id: ' + req.body._id);
  if(req.body._id){
    var b = await Bicicleta.deleteById(req.body._id);
    res.status(200).json({status: b});
  } else{
    res.status(500).json({error: `not found document by _id: ${req.body._id}`})
  } 
}

/*
exports.bicicleta_list = function(req, res){
  res.status(200).json({
    bicicletas: Bicicleta.allBicis
  });
}

exports.bicicleta_create = function(req, res){
  var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.add(bici);

  res.status(200).json({
    bicicleta: bici
  });
}

exports.bicicleta_update = function(req, res){
  var bici = Bicicleta.findById(req.body.id);
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];
  
  res.status(200).json(bici);
}

exports.bicicleta_delete = function(req, res){
  Bicicleta.removeById(req.body.id);
  res.status(200).send();
} */