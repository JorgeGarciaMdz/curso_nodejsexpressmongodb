var Usuario = require('../models/usuarioModel');
var Token = require('../models/tokenModel');

module.exports = {
  confirmationGet: function(req, res, next){
    Token.findOne({token: req.params.token}, (err, token)=>{
      if(!token){
        return res.status(400).send({type: 'not-verified', msg: 'No se encontro usuario con este token. Debe solicitarlo nuevamente'});
      } else{
        Usuario.findById(token._userId, (err, usuario)=>{
          if(!usuario){
            return res.status(400).send({msg: 'No se encontro usuario con el token relacionado'});
          }
          if(usuario.verificado){
            return res.redirect('/usuarios');
          }
          usuario.verificado = true;
          usuario.save((err)=>{
            if(err){
              return res.status(500).send({msg: err.message});
            }
            res.redirect('/');
          });
        });
      }
    });
  }
}