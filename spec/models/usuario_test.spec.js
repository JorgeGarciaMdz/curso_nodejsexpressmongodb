var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletaModel');
var Usuario = require('../../models/usuarioModel');
var Reserva = require('../../models/reservaModel');
const connectRedBicicletaDB = require('../connectionDB/conectionMongoDB');

describe('Testing Usuarios', ()=>{
  beforeEach((done)=>{
    const connectDB = connectRedBicicletaDB.getConnectRedBicicletas();
    done();
  });

  afterEach((done)=>{
    Reserva.deleteMany({}, (err, success)=>{
      if(err) console.log(err);
      Usuario.deleteMany({}, (err, success)=>{
        if(err) console.log(err);
        Bicicleta.deleteMany({}, (err, success)=>{
          if(err) console.log(err);
          done();
        });
      });
    });
  });

  describe('Usuario reserva una bicicleta', ()=>{
    it('Debe existir la reserva', (done)=>{
      const usuario = new Usuario({nombre: "claro"});
      const bicicleta = Bicicleta.createInstance(1, "negro", "playera", [44, 44]);
      bicicleta.save({}, (err, success)=>{
        if(err) console.log(err);
        console.log(success);
      });

      let hoy = new Date();
      let mañana = new Date();
      mañana.setDate(hoy.getDate() + 1);
      usuario.reservar(bicicleta._id, hoy, mañana, (err, reserva)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
        } else{
          Reserva.find({}).populate('Bicicleta').populate('Usuario').exec((err, reservas)=>{
            if(err){
              console.error.bind(console, 'Ocurred an error');
            } else{
              console.log(reservas[0]);
              expect(reservas.length).toBe(1);
              expect(reservas[0].diasDeReserva()).toBe(2);
              expect(reservas[0].bicicleta).toEqual(bicicleta._id);
              expect(reservas[0].usuario).toEqual(usuario._id);
              done();
            }
          });
        }
      });
    });
  });
});