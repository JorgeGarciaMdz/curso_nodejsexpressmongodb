'use strict'
const mongoose = require('mongoose');
const db_link = 'mongodb://localhost/red_test';
var db = '';

function getConnectRedBicicletas(){
  mongoose.connect(db_link, {useNewUrlParser: true});
  db = mongoose.connection;
  db.on('error', console.error.bind(console, 'error connection'));
  db.once('open', function(){
    console.log('----> MongoDB connection sussefull <-----');
  });
  return db;
}

exports.getConnectRedBicicletas = getConnectRedBicicletas;