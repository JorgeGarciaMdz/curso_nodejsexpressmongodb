require('dotenv').config();
const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

/*const mailConfig = {
  host: 'smtp.ethereal.email',
  port: 587,
  auth: {
    user: 'dberyl.murazik34@ethereal.email',
    pass: 'ZT68v1tcQ4jNSYbbWC'
  }
} */

/*module.exports = {   // anda
  sendMail: async function main(options){
    let testAccount = await nodemailer.createTestAccount();
    let transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: 'beryl.murazik34@ethereal.email',
        pass: 'ZT68v1tcQ4jNSYbbWC'
      }
    });

    let info = await transporter.sendMail(options);

    console.log("message send: %s", info.messageId);

    console.log("prev url: %s", nodemailer.getTestMessageUrl(info));
  } 
} */

var mailConfig;
if(process.env.NODE_ENV === 'production'){
  const options = {
    auth: {
      //api_key: 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
      api_key: process.env.SENGRID_API_SECRET
    }
  }
  mailConfig = sgTransport(options);
  console.log('in production mail options: ' + options);
} else {
  if(process.env.NODE_ENV === 'staging'){
    const options = {
      auth: {
        api_key: process.env.SENGRID_API_SECRET
      }
    }
    mailConfig = sgTransport(options);
    console.log('in staging mail options: ' + options );
  } else {
    // all email are catched by ethereal.email
    mailConfig = {
      host: 'smtp.ethereal.email',
      port: 587,
      //secure: 'STARTTLS',
      auth: {
        user: process.env.ethereal_user, //'kendall.hagenes@ethereal.email', //desde variable de entorno .env
        pass: process.env.ethereal_pwd //'WWfAp55NFSw3P4PAyd'
      }
    }
    console.log('in dev local, mailConfig: ' + mailConfig);
  }
}

module.exports = {
  sendMail: async function(message){
    // Generate SMTP service account from ethereal.email
    console.log('entorno: ' + process.env.NODE_ENV); 
    nodemailer.createTestAccount((err)=>{
      if(err){
        console.error('Failed to create a testing account. ' + err.message);
        return process.exit(1);
      }
      console.log('Credential obtained, send message...');

      // Create a SMTP transporter object
      /*let transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        //secure: 'STARTTLS',
        auth: {
          user: 'kendall.hagenes@ethereal.email',
          pass: 'WWfAp55NFSw3P4PAyd'
        }
      }); */
      let transporter = nodemailer.createTransport(mailConfig);

      transporter.sendMail(message, (err, info)=>{
        if(err){
          console.log("Error ocurred in sendMail. Error: " + JSON.stringify(err));
          //return process.exit(1);
          return;
        }

        console.log('Message sent: %s' + info.messageId);
        console.log('Preview URL: %s' + nodemailer.getTestMessageUrl(info));
      })
    })
  }
}