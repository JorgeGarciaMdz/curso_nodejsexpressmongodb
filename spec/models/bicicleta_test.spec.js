var mongoose = require('mongoose');
const connectRedBicicletaDB = require('../connectionDB/conectionMongoDB');


var Bicicleta = require('../../models/bicicletaModel');

var timeJasmine;

describe('Testing Bicicletas', ()=>{
  beforeEach((done)=>{
    const connectDB = connectRedBicicletaDB.getConnectRedBicicletas();
    timeJasmine = jasmine.DEFAULT_TIMEOUT_INTERVA;
    jasmine.DEFAULT_TIMEOUT_INTERVA = 20000;
    done();
  });

  afterEach((done)=>{
    Bicicleta.deleteMany({}, (error, success )=>{
      if(error) console.log(error);
    });
    jasmine.DEFAULT_TIMEOUT_INTERVA = timeJasmine;
    done();
  }); 

  describe('Bicicleta.createInstance', ()=>{
    it('Crea una instancia de bicicleta', (done)=>{
      var bici = Bicicleta.createInstance(1, 'verde', 'playera', [2,3]);
      expect(bici.code).toBe(1);
      expect(bici.color).toBe('verde');
      expect(bici.modelo).toBe('playera');
      expect(bici.ubicacion[0]).toBe(2);
      expect(bici.ubicacion[1]).toBe(3);
      done();
    });
  });

  describe('Bicicleta.findAll', ()=>{
    it('Lista vacia', (done)=>{
      Bicicleta.findAll((err, bicis)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
        } else{
          expect(bicis.length).toBe(0);
          done();
        }
      });
    });
  });  

  describe('Bicicleta.add', ()=>{
    it('Agrega un objeto bicicleta', (done)=>{
      let aBici = Bicicleta.createInstance(1, 'rojo', 'playera', [2,3]);
      Bicicleta.add(aBici, (err, newBici)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
        } else{
          console.log(newBici);
          Bicicleta.findAll((err, bicis)=>{
            if(err){
              console.error.bind(console, 'Ocurred an error');
            } else{
              console.log(bicis);
              expect(bicis.length).toBe(1);
              expect(bicis[0].code).toBe(aBici.code);
              done()
            }
          });
        }
      });
    });
  });

  describe('Bicicleta.findByCode', ()=>{
    it('Devuelve objeto bicicleta con code = 1', (done)=>{
      Bicicleta.findAll((err, bicis)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
        } else{
          expect(bicis.length).toBe(0);
          
          let aBici_1 = Bicicleta.createInstance(1, 'verde', 'playera', [6,7]);
          Bicicleta.add(aBici_1, (err, newBici)=>{
            if(err){
              console.error.bind(console, 'Ocurred an error');
            } else{
              console.log(newBici);
              let aBici_2 = Bicicleta.createInstance(2, 'rojo', 'urbana', [0, 9]);
              Bicicleta.add(aBici_2, (err, newBici2)=>{
                if(err){
                  console.error.bind(console, 'Ocurred an error');
                } else{
                  console.log(newBici2);
                  Bicicleta.findByCode(1, (err, targetBici)=>{
                    if(err){
                      console.error.bind(console, 'Ocurred an error');
                    } else{
                      console.log(targetBici);
                      expect(targetBici.code).toBe(aBici_1.code);
                      expect(targetBici.color).toEqual(aBici_1.color);
                      expect(targetBici.modelo).toEqual(aBici_1.modelo);
                      done();
                    }
                  });
                }
              });
            }
          });
        }
      });
    });
  });

  describe('Bicicletas.removeByCode', ()=>{
    it('Elimina un objeto bicicleta con el code', (done)=>{
      Bicicleta.findAll((err, bicis)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
        } else{
          console.log(bicis);
          expect(bicis.length).toBe(0);
          bici_1 = Bicicleta.createInstance(1, 'rojo', 'playera', [0,0]);
          Bicicleta.add(bici_1, (err, newBici1)=>{
            if(err){
              console.error.bind(console, 'Ocurred an error');
            } else{
              console.log(newBici1);
              Bicicleta.findAll((err, bicis)=>{
                if(err){
                  console.error.bind(console, 'Ocurred an error');
                } else{
                  console.log(bicis.length);
                  expect(bicis.length).toBe(1);
                  let bici_2 = Bicicleta.createInstance(2, 'verde', 'urban', [1, 1]);
                  Bicicleta.add(bici_2, (err, newBici2)=>{
                    if(err){
                      console.error.bind(console, 'Ocurred an error');
                    } else{
                      console.log(newBici2);
                      Bicicleta.removeByCode(1, (err, rmBici1)=>{
                        if(err){
                          console.error.bind(console, 'Ocurred an error');
                        } else{
                          console.log('Delete one:  ' + JSON.stringify(rmBici1));
                          Bicicleta.findAll((err, bicis)=>{
                            if(err){
                              console.error.bind(console, 'Ocurred an error');
                            } else {
                              console.log(bicis);
                              expect(bicis.length).toBe(1);
                              expect(bicis[0].code).toBe(bici_2.code);
                              expect(bicis[0].color).toEqual(bici_2.color);
                              expect(bicis[0].modelo).toEqual(bici_2.modelo);
                              done();
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    });
  });
 
  /*describe('Bicicleta.findByCode', ()=>{
    it('Devuelve bici con un codigo', (done)=>{
      Bicicleta.allBicis(function(error, Bicis){
        expect(Bicis.length).toBe(0);

        var aBici1 = new Bicicleta({code: 1, color: "yellow", modelo: "playera"});
        Bicicleta.add(aBici1, function(error, newBici){
          if(error) console.log(error);

          var aBici2 = new Bicicleta({code: 2, color: "marron", modelo: "urbana"});
          Bicicleta.add(aBici2, function(error, newBici2){
            if(error) console.log(error);

            Bicicleta.findByCode(1, function(error, targetBici){
              expect(targetBici.code).toBe(aBici1.code);
              expect(targetBici.color).toBe(aBici1.color);
              expect(targetBici.modelo).toBe(aBici1.modelo);
              done();
            });
          });
        });
      });
    });
  }); */

});



/*
beforeEach(()=>{
  Bicicletas.allBicis = [];
});

describe('Bicicletas.allBicis', ()=>{
  it('Comienza vacio', ()=>{
    expect(Bicicletas.allBicis.length).toBe(0);
  });
});

//agregar bicicleta
describe('Bicicletas.add', ()=>{
  it('Agregar una', ()=>{
    expect(Bicicletas.allBicis.length).toBe(0);

    var bici = new Bicicletas(1, "rojo", "playera", [-1.2, -3,4]);
    Bicicletas.add(bici);

    expect(Bicicletas.allBicis.length).toBe(1);
    expect(Bicicletas.allBicis[0]).toEqual(bici);
  });
});

//findById
describe('Bicicletas.findById', ()=>{
  it('Debe devolver Bicicleta con Id: 1', ()=>{
    expect(Bicicletas.allBicis.length).toBe(0);
    var aBici_1 = new Bicicletas(1, "blanco", "urbana");
    var aBici_2 = new Bicicletas(1, "negro", "urbana");
    Bicicletas.add(aBici_1);
    Bicicletas.add(aBici_2);
    var targetBici = Bicicletas.findById(1);
    expect(targetBici.id).toBe(aBici_1.id);
    expect(targetBici.color).toBe(aBici_1.color);
    expect(targetBici.modelo).toBe(aBici_1.modelo);
  });
});

//removeById
describe('Bicicletas.removeById', ()=>{
  it('Debe eliminar un objeto bicicleta con id: 1', ()=>{
    expect(Bicicletas.allBicis.length).toBe(0);
    var aBici = new Bicicletas(1, "negro", "urbana");
    Bicicletas.add(aBici);
    expect(Bicicletas.allBicis.length).toBe(1);
    Bicicletas.removeById(aBici.id);
    expect(Bicicletas.allBicis.length).toBe(0);
  });
}); */