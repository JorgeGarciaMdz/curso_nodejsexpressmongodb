require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletasRoute');
var bicicletasApiRouter = require('./routes/api/bicicletasRouterApi');
var usuarioRouterApi = require('./routes/api/usuarioRouterAPI');
var usuarioRouter = require('./routes/usuarioRouter');
var tokenRouter = require('./routes/tokenRouter');
var authAPIRouter = require('./routes/api/authRouterApi');

const connectRedBicicletaDB = require('./conectionDB/conectionMongoDB');
const connectDB = connectRedBicicletaDB.getConnectRedBicicletas();

const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const { token } = require('morgan');

let store;
if(process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'session'
  });
  store.on('error', function(err){
    assert.ifError(err);
    assert.ok(false);
  });
}

const Token = require('./models/tokenModel');
const Usuario = require('./models/usuarioModel');

const jwt  = require('jsonwebtoken');

var app = express();
/*
//configuracion de mongoose
var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDB, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongoDB coneccion error:'));
db.once('open', ()=>{
  console.log('mongodb connect sussefull');
});  */

app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  //opciones
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: '...red--bicicletas...'
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', indexRouter);
app.use('/usuarios', usuarioRouter);
app.use('/token', tokenRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasApiRouter);
//app.use('/api/usuarios', validarUsuario, usuarioRouterApi);
app.use('/api/usuarios', usuarioRouterApi);
app.use('/api/auth', authAPIRouter);

app.get('/login', function(req, res){
  res.render('session/login');
});

app.post('/login', function(req, res, next){
  passport.authenticate('local', function(err, usuario, info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res){
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    if(!usuario) return res.render('session/forgotPassword', {info: {message: 'no hay usuario vinculado al email'}});
    usuario.resetPassword(function(err){
      if(err) return next(err);
      console.log('session/forgotPasswordMessage');
    });
    res.render('session/forgotPasswordMessage')
  });
});

app.get('/resetPassword/:token', function(req, res, next){
  /*Token.findOne({ token: req.params.token}, function(err, token){
    if(!token) return res.status(400).send({ type: 'not-verified', msg: 'No existe usuario asociado al token'});
    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({msg: 'No existe usuario asociado a dicho token'});
      res.render('session/resetPassword', {errrs: {}, usuario: usuario});
    });
  }); */
  Usuario.findOne({passwordResetToken: req.params.token}, function(err, usuario){
    if(err) console.log(err.message);
    if(!usuario) return res.status(400).send({type: 'not-verified', msg: 'No existe usuario asociado a token'});
    if(usuario.passwordResetTokenExpires > Date.now) return res.status(400).send({type: 'not-verified', msg: 'Token a expirado'});
    res.render('session/resetPassword', {errors: { password: ''}, usuario: usuario})
  });
});

app.post('/resetPassword', function(req, res){
  if(req.body.password != req.body.confirm_password){
    res.render('session/resetPassword', {errors: {confirm_password: { message: 'Las contraseñas no coinciden'}}});
    return ;
  }
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    if(err) console.log(err.message);
    else{
      usuario.password = req.body.password;
      usuario.passwordResetToken = '';
      usuario.passwordResetTokenExpires = null;
      usuario.save(function(err){
        if(err){
          res.render('session/resetPassword', {errors: err.message, usuario: usuario});
        } else{
          res.redirect('/login');
        }
      });
    }
  });
});

app.use('/privacy_policy', function(req, res){
  res.sendFile(path.resolve('public/privacity_policy.html'), function(err){
    if(err) { 
      console.log("dont send policy, error: ");
      res.status(400).send();
    } 
    return;
  });
});

app.use('/googlecb07f4c8855b5b01', function(req, res){
  res.sendFile(path.resolve('public/googlecb07f4c8855b5b01.html'), function(err){
    if(err){
      console.log('dont sent key google');
      res.status(400).send();
    }
    return;
  });
});

app.get('/auth/google', 
  passport.authenticate('google', { scope: [
    /*'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.profile.emails.read'*/
  'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/userinfo.email'
  ]}
    )
);

app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/error'
}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if(req.user){
    next();
  } else{
    console.log('Usuario sin loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  console.log(req.headers['x-access-token']);
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err){
      console.log('in error')
      res.json({status: "error", message: err.message, data: null});
    } else{
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}

module.exports = app;
