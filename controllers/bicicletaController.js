var Bicicleta = require('../models/bicicletaModel');

exports.bicicleta_list = async function(req, res){
  //res.render('bicicletas/index', {bicis: await Bicicleta.findAll()})
  Bicicleta.findAll((err, listBcis)=>{
    if(err){
      console.error.bind(console, 'Ocurred an error');
      res.status(404).send();
    } else{
      console.log('findAllBicis: ' + listBcis);
      res.render('bicicletas/index', {bicis: listBcis});
    }
  });
}

exports.bicicletas_create_get = function(req, res){
  res.render('bicicletas/create');
}

exports.bicicletas_create_post= function(req, res){
  var b = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);

  b.save(function(err, bici){
    if(err) console.error.bind(console, 'Error!');
    console.log('its ok ' + bici);
    res.redirect('/bicicletas');
  });
}

exports.bicicletas_update_get = function(req, res){
  Bicicleta.findByCode(req.params.id, (err, bici)=>{
    if(err){
      console.error.bind(console, 'Ocurred an error');
      res.status(404).send();
    } else{
      console.log(bici);
      res.render('bicicletas/update', {bici: bici});
    }
  }); 
}

exports.bicicletas_update_post= async function(req, res){
  Bicicleta.findByCode(req.params.id, (err, bici)=>{
    if(err){
      console.error.bind(console, 'Ocurred an error');
      res.status(404).send();
    } else{
      bici.id = req.body.id;
      bici.color = req.body.color;
      bici.modelo = req.body.modelo;
      bici.ubicacion = [req.body.lat, req.body.lng];

      bici.save((err, updateBici)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
          res.status(404).send();
        } else{
          console.log('its ok ' + updateBici);
          res.redirect('/bicicletas');
        }
      });
    }
  });
}

exports.bicicletas_delete_post = function(req, res){
  Bicicleta.deleteById(req.body._id);
  res.redirect('/bicicletas');
}