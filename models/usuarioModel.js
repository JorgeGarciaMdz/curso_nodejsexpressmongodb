var mongoose = require('mongoose');
var Reserva = require('./reservaModel');
var Schema = mongoose.Schema;

const Token = require('./tokenModel');
const mailer = require('../mailer/mailer');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;

const validateEmail = function(email){
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
}

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true, //para que no haya espacios vacios al principio y al final
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es obligatorio'],
    lowercase: true,
    unique: true,
    validate: [validateEmail, 'Por favor ingrese un Email Valido'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio'],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false
  },
  googleId: String,
  facebookId: String
});

usuarioSchema.statics.createInstance = function(nombre, email, password){
  return new this({
    nombre: nombre,
    email: email,
    password: password
  });
}

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'}); // referencia al atributo email

usuarioSchema.pre('save', function(next){
  if(this.isModified('password')){
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
}); 

usuarioSchema.methods.validPassword = function(password){
  return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
  var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
  console.log(reserva);
  reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
  const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
  const email_destination = this.email;
  token.save((err)=>{
    if(err){
      return console.log(err);
    } else{
      const mailOptions = {
        from: 'no-reply@redbicicletas.com',
        to: email_destination,
        subject: 'Verificacion de cuenta',
        text: 'Hola.\n\n' + 'Por favor, para verificar su cuenta, haga click en este link:\n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token +'\n'
      };
      
      /*mailer.sendMail(mailOptions, function (err){
        if(err){
          return console.log(err.message);
        } else{
          console.log('A verification email has been sent to ' + email_destination + '.');
        }
      });  */
      mailer.sendMail(mailOptions);
    }
  });
}

usuarioSchema.methods.resetPassword = function(cb){
  this.passwordResetToken = crypto.randomBytes(16).toString('hex');
  this.passwordResetTokenExpires = Date.now() + 42000;
  console.log('in password reset');
  this.save(function(err, user){
    if(err) return console.log(err.message);
      const mailOptionsResetPassword = {
        from: 'no-reply@redbicicletas.com',
        to: user.email,
        subject: 'Recuperacion contraseña',
        text: 'Hola.\n\n' + 'Por favor, para recupera su contraseña, haga click en este link:\n' + 
              'http://localhost:3000' + '\/resetPassword\/' + user.passwordResetToken +'\n'
      };
      mailer.sendMail(mailOptionsResetPassword);
      cb(err);
  });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
  const self = this;
  console.log('--> in usuarioModel.findOneOrCreateByGoogle' + JSON.stringify(condition));
  console.log('--> condition: ' + condition);
  self.findOne({
    $or: [
      {'googleId': condition.id},
      {'email': condition.emails[0].value}
    ]
  }, (err, result)=>{
    if(result){
      callback(err, result);
    } else {
      console.log('======= CONDITION =======');
      console.log(condition);
      let values = {};
      values.googleId = condition.id;
      values.email = condition.emails[0].value;
      values.nombre = condition.displayName || 'SIN NOMBRE';
      values.verificado = true;
      //values.password = condition._json.etag;
      //values.password = condition.displayName;
      values.password = condition._json.etag || crypto.randomBytes(16).toString('hex');
      console.log('====== VALUES =====');
      console.log(values);
      self.create(values, (err, result)=>{
        if(err) {console.log(err)}
        return callback(err, result);
      });
    }
  });
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
  const self = this;
  console.log('--> in usuarioModel.findOneOrCreateByFacebook' + JSON.stringify(condition));
  console.log('--> condition: ' + condition);
  self.findOne({
    $or: [
      {'facebookId': condition.id},
      {'email': condition.emails[0].value}
    ]
  }, (err, result)=>{
    if(result){
      callback(err, result);
    } else {
      console.log('======= CONDITION =======');
      console.log(condition);
      let values = {};
      values.facebookId = condition.id;
      values.email = condition.emails[0].value;
      values.nombre = condition.displayName || 'SIN NOMBRE';
      values.verificado = true;
      //values.password = condition._json.etag;
      values.password = condition._json.etag || crypto.randomBytes(16).toString('hex');
      console.log('====== VALUES =====');
      console.log(values);
      self.create(values, (err, result)=>{
        if(err) {console.log(err)}
        return callback(err, result);
      });
    }
  });
}

usuario = mongoose.model('Usuario', usuarioSchema);
module.exports = usuario;