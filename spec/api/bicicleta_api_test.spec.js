var Bicicleta = require('../../models/bicicletaModel');
var request = require('request');
var server = require('../../bin/www');
//var mongoose = require('mongoose');

describe('Testing API bicicletas', ()=>{
  beforeEach((done)=>{
    /* No se realiza otra conexion a la DB debido a que el server ya esta conectado
    var urlDb = 'mongoDB://localhost/test';
    mongoose.connect(urlDb, {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', ()=>{
      console.error.bind(console, 'Ocurred an error at connect db');
    });
    db.once('open', ()=>{
      console.log('Conection succefull');
    }); */
    done();
  });

  afterEach((done)=>{
    Bicicleta.deleteMany({}, (err, success)=>{
      if(err) console.error.bind(console, 'Ocurred an Error of connection');
    });
    done();
  });

  describe('GET all Bicicletas', ()=>{
    it('Status 200', (done)=>{
      request.get('http://localhost:3000/api/bicicletas', function(err, res, body){
        expect(res.statusCode).toBe(200);
        bici = JSON.parse(body);
        Bicicleta.findAll((err, bicis)=>{
          if(err){
            console.error.bind(console, 'Ocurred an error');
          } else{
            console.log(bicis);
            expect(bici.bicicletas.length).toBe(bicis.length);
            done();
          }
        });
      });
    });
  });

  describe('POST API/BICICLETA/CREATE', ()=>{
    it('STATUS 200', (done)=>{
      var headers = {'content-type' : 'application/json'};
      var aBici = '{"code": 1, "color": "negro", "modelo": "playera", "lat": -34, "lng": -64}';
      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bicicletas/create',
        body: aBici
      }, function(error, res, body){
        if(error){
          console.error.bind(console, 'Ocurred an error');
        } else{
          expect(res.statusCode).toBe(200);
          biciPost = JSON.parse(body);
          Bicicleta.findByCode(biciPost.bicicletas.code, (err, bici)=>{
            if(err){
              console.error.bind(console, 'Ocurred an error');
            } else{
              expect(biciPost.bicicletas.code).toBe(bici.code);
              expect(biciPost.bicicletas.color).toEqual(bici.color);
              expect(biciPost.bicicletas.modelo).toEqual(bici.modelo);
              done();
            }
          });
        }
      });
    });
  });

  describe('GET API/BICICLETAS/CODE', ()=>{
    it('status 200', (done)=>{
      var headers = {'content-type' : 'application/json'};
      var biciCode = '{"code": 1}';
      var aBici=  Bicicleta.createInstance(1, "negro", "playera", [-34,-64]);
      Bicicleta.add(aBici, (err, newBici)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
        } else{
          request.get({
            headers: headers,
            url: 'http://localhost:3000/api/bicicletas/code',
            body: biciCode
          }, (err, res, body)=>{
            if(err){
              console.error.bind(console, 'Ocurred an error');
            } else{
              bici = JSON.parse(body);
              expect(res.statusCode).toBe(200);
              expect(newBici.code).toBe(bici.bicicleta.code);
              expect(newBici.color).toEqual(bici.bicicleta.color);
              expect(newBici.modelo).toEqual(bici.bicicleta.modelo);
              done();
            }
          });
        }
      });
    });
  }); 

  describe('DELETE /API/BICICLETAS/DELETE', ()=>{
    it('status 200', (done)=>{
      //var headers = {'contentType' : 'application/json'};
      //var headers = {'content-type' : 'application/json'};
      var headers = {'Content-Type': 'application/json'};
      var aBici=  Bicicleta.createInstance(1, "negro", "playera", [-34,-64]);
      Bicicleta.add(aBici, (err, newBici)=>{
        if(err){
          console.error.bind(console, 'Ocurred an error');
        } else{
          console.log(newBici);
          Bicicleta.findAll((err, allBicis1)=>{
            if(err){
              console.error.bind(console, 'Ocurred an error');
            } else{
              console.log(allBicis1);
              expect(allBicis1.length).toBe(1);
              expect(allBicis1[0].code).toBe(aBici.code);
              expect(allBicis1[0].color).toEqual(aBici.color);
              expect(allBicis1[0].modelo).toEqual(aBici.modelo);

              var bodyId = '{"_id":' + allBicis1[0]._id + '}';
              console.log("--->>" + bodyId);
              request({
                method: 'DELETE',
                url: 'http://localhost:3000/api/bicicletas/delete',
                /*headers: {
                  'Accept': 'application/json',
                  'Accept-Charset': 'utf-8'
                }, */
                headers: headers,
                body: bodyId  
              }, (err, res, body)=>{
                console.log("->-> " + res.statusCode);
                Bicicleta.findAll((err, bicis)=>{
                  if(err){
                    console.error.bind(console, 'Ocurred an error');
                  } else{
                    console.log(bicis);
                    //expect(bicis.length).toBe(0);
                    done();
                  }
                });
              });
            }
          });
        }
      });
    });
  });
}); 

/*
beforeEach(()=>{
  Bicicleta.allBicis = [];
});

describe('Bicicleta API', ()=>{
  describe('GET BICICLETAS', ()=>{
    it('Status 200', ()=>{
      expect(Bicicleta.allBicis.length).toBe(0);

      var a = new Bicicleta(1, "negro", "playera", -34, -63);
      Bicicleta.add(a);

      request.get('http://localhost:3000/bicicletas', function(error, response, body){
        expect(response.statusCode).toBe(200);
      });
    });
  });
});

describe('POST BICICLETAS/CREATE', ()=>{
  it('STATUS 200', (done)=>{
    var headers = {'content-type' : 'application/json'};
    var aBici = '{"id": 1, "color": "negro", "modelo": "playera", "lat": -34, "lng": -64}';

    request.post({
      headers: headers,
      url: 'http://localhost:3000/api/bicicletas/create',
      body: aBici
    }, function(error, response, body){
      expect(response.statusCode).toBe(200);
      expect(Bicicleta.findById(1).color).toBe("negro");
      done();
    });
  });
});

describe('PUT BICICLETAS/UPDATE', ()=>{
  it('STATUS 200', (done)=>{
    var headers = {'content-type' : 'application/json'};
    var aBici = '{"id": 1, "color": "gray", "modelo": "playera", "lat": -34, "lng": -64}';
    
    Bicicleta.add(new Bicicleta(1, "negro", "playera", -34, -64));

    request.put({
      headers: headers,
      url: 'http://localhost:3000/api/bicicletas/update',
      body: aBici
    }, function(error, response, body){
      expect(response.statusCode).toBe(200);
      expect(Bicicleta.findById(1).color).toBe("gray");
      done();
    });
  });
});

describe('DELETE BICICLETAS/DELETE', ()=>{
  it('STATUS 200', (done)=>{
    var headers = {'content-type' : 'application/json'};
    var body = '{"id": 1}';

    Bicicleta.add(new Bicicleta(1, "negro", "playera", -34, -64));

    request.delete({
      headers: headers,
      url: 'http://localhost:3000/api/bicicletas/delete',
      body: body
    }, function(error, response, body){
      expect(response.statusCode).toBe(200);
      expect(Bicicleta.allBicis.length).toBe(0);
      done();
    });
  });
}); */