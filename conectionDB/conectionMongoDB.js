'use strict'
require('dotenv').config();
const mongoose = require('mongoose');
//const db_link = 'mongodb://localhost/red_bicicletas';
//mongodb+srv://admin:<password>@cluster0.l05dh.mongodb.net/<dbname>?retryWrites=true&w=majority
// conexion mongo atlas  <password>: NjLxq8KUKzfUos0F
//const db_link = 'mongodb+srv://admin:NjLxq8KUKzfUos0F@cluster0.l05dh.mongodb.net/red-bicicletas?retryWrites=true&w=majority';
// usando el .env que se obtiene al importar 'dotenv
const db_link = process.env.MONGO_URI;
var db = '';

function getConnectRedBicicletas(){
  mongoose.connect(db_link, {useNewUrlParser: true});
  //mongoose.Promise = globa.Promise;
  db = mongoose.connection;
  db.on('error', console.error.bind(console, 'error connection'));
  db.once('open', function(){
    console.log('--> connection sussefull to -> ' + db_link);
  });
  return db;
}

exports.getConnectRedBicicletas = getConnectRedBicicletas;