var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const BicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
  type: [Number], index: {type : '2dsphere', sparse: true } 
  } 
});

BicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion
  });
}

BicicletaSchema.statics.findAll = function(cb){
  return this.find({}, cb);
}

BicicletaSchema.statics.add = function(aBici, cb){
  aBici.save(cb);
}

BicicletaSchema.statics.findByCode =  function(aCode, cb){
  return this.findOne({code: aCode}, cb);
}

BicicletaSchema.statics.findById = function(_id, cb){
  return this.findOne({_id: _id}, cb);
}

BicicletaSchema.statics.removeByCode = function(aCode, cb){
  return this.deleteOne({code: aCode}, cb);
}

BicicletaSchema.statics.deleteById = async function(_id){
  return await Bicicleta.deleteOne({_id: _id});
}

BicicletaSchema.methods.toString = function(){
  return "code: " + this.code + ", color: " + this.color + ", modelo: " + this.modelo;
}

const Bicicleta = new mongoose.model('Bicicleta', BicicletaSchema);

module.exports = Bicicleta;