var express = require('express');
var router = express.Router();
var usuarioControllerApi = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioControllerApi.usuario_list);
router.post('/create', usuarioControllerApi.usuario_create);
router.post('/reservar', usuarioControllerApi.usuario_reserva);

module.exports = router;